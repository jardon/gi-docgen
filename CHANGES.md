# Changes

All notable changes to this project will be documented in this file.

## [Unreleased]

### Added

### Changed

### Fixed

### Removed

## [2023.1] - 2023-01-06

### Added

- Use tomlib for Python >= 3.11 and tomli/toml for Python < 3.11 [!168, !172]

### Fixed

- Use the proper link fragment for interface prerequisite [#148]
